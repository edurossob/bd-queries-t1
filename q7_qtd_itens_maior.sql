select n_name, sum(l_quantity) as total
    from lineitem
    join orders on l_orderkey=o_orderkey
    join customer on c_custkey=o_custkey 
    join nation on c_nationkey=n_nationkey 
    group by n_name 
    having total>62000
    order by total desc;
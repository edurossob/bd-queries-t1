select count(distinct(o_custkey)) 
    from ORDERS as o1
    where o_comment like "%special request%"
    and not exists (
        select o_custkey 
        from ORDERS as o2
        where o1.o_custkey=o2.o_custkey 
        and o_comment like "%unusual package%"
    );
Select count(c_custkey)
    from(
        select c_custkey
        from customer
        EXCEPT
        select c_custkey
        from customer join orders
        on c_custkey=o_custkey
    );
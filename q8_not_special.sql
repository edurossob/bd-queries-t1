select count(distinct(o_custkey)) 
    from ORDERS as a 
    where not exists (
        select o_custkey 
        from ORDERS as o 
        where a.o_custkey=o.o_custkey 
        and o_comment like "%special request%"
    );
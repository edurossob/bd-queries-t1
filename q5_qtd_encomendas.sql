select n_name, count(n_nationkey) as total
    from orders 
    join customer on c_custkey=o_custkey 
    join nation on c_nationkey=n_nationkey 
    group by n_name 
    order by total desc;